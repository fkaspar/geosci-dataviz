# Exploration of Geoscientific Data Sets

This repo is intended as a a collection short exploratory analyses and data visualizations of different data sets from the realm of earth system sciences.

- Average temperature measured at Sonnblick Observatory » 1887-2022
- Arctic Sea Ice Volume (PIOMAS Reanalysis) » 1979-2022
